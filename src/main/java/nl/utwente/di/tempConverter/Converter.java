package nl.utwente.di.tempConverter;

public class Converter {
    public Converter() {
    }

    public double getFahrenheit(String celsius) {
        return Double.parseDouble(celsius) * 9 / 5 + 32;
    }
}
